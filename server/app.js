console.log("Hello World!");

console.log("------- SWITCH ------");

var y = 11;

switch(true) {
    case (y == 11):
        console.log("It's 11");
        break;
    case (y < 11):
        console.log("Less than 11");
        break;
    case (y > 11):
        console.log("More than 11");
        break;
    default:
        console.log("Default");
}

console.log("------- WHILE ------");

var z = 0;

while(z < 10) {
    console.log("hello");
    z++;
}

console.log("------- ARRAY ------");

var fruits = [ "Watermelon", "Durian", "Apple", "Orange"];

for (var x = 0; x < fruits.length; x++) {
    console.log(fruits[x]);
}

fruits.forEach(function(value, index) {
    console.log(index);
    console.log(value);
})

console.log("------- INDEX OF ------");
var posArray = fruits.indexOf("Durian");
console.log(posArray);

console.log(fruits);

console.log("------- SLICE 1, 3 ------");
var sliced = fruits.slice(1,3);
console.log(sliced);

console.log("------- SHIFT ------");
fruits.shift();
console.log(fruits);

console.log("------- SORT ------");
fruits.sort();
console.log(fruits);

console.log("------- RELATIONSHIPS ------");

var author = {
    name: "Kenneth"
}

var publisher = {
    house: "Penguin"
}

var releaseDate = [ {date: 1990, revision: 1}, {date: 2010, revision: 2}, 2012 ]

var latestBook = {
    title: "Learn ABC",
    author: author,
    publisher: publisher,
    publishDate: releaseDate
}

console.log(latestBook); 

delete latestBook.publishDate;
console.log(latestBook);

console.log("------- || DEFAULT ------");

function defaultParam (param1) {
    var paramIn = param1 || 4000;  
    console.log(">> " + paramIn);
}

defaultParam(3000);

console.log("------- CONCATANATION ES6 ------");

var fullName = "Kenneth " + "Phang " + "Tak" ;
console.log(fullName);

var full_name = "Kenneth Phang Tak";
var age = 45 + 7; // ABILITY TO COMPUTE 

var fullName = `${full_name} ` + `${age}`;
console.log(fullName);


